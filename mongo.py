import pymongo
from bson.objectid import ObjectId


class MyMongoClient(object):

  def __init__(self, hostname, port, **kwargs):
    self.hostname = hostname
    self.port = port
    self.connection_args = kwargs
    self.client = None
    self.table = None
    self.index = None

  def connect(self):
    self.client = pymongo.MongoClient(self.hostname, self.port, **self.connection_args)

  def close(self):
    self.client.close()

  def index_exists(self, index):
    return self.table.find_one({self.index: index}) is not None

  def get_data(self, index):
    return self.table.find_one({self.index: index})

  def get_doc_info(self, index):
    return self.table.find_one({self.index: index}, {self.index: 1, "_id": 1})

  def list_docs(self, cond=None, ascending=True):
    if cond is None:
      cond = {}
    if ascending:
      sort = pymongo.ASCENDING
    else:
      sort = pymongo.DESCENDING

    return list(self.table.find(cond, {self.index: 1, "_id": 0}, sort = [(self.index, sort)]))

  def prepare_update(self, index):
    info = self.get_doc_info(index)
    if not info:
      return None, None
    old_data = self.get_data(index)
    del old_data["_id"]
    return old_data, ObjectId(info["_id"])

  def complete_update(self, objID, updated_data):
    return self.table.update_one({"_id": objID}, {"$set": updated_data}).matched_count

  def insert_data(self, data):
    doc = self.table.insert_one(data)
    if doc:
      return doc.inserted_id
    else:
      return None


class CIClient(MyMongoClient):
  def __init__(self, hostname, port, **kwargs):
    super().__init__(hostname, port, authSource="CI", **kwargs)
    self.index = "rev"

  def connect(self):
    super().connect()
    self.table = self.client["CI"]["rev_metrics"]

  def update_revision(self, rev, data, src_rev=None):
    old_data, objID = self.prepare_update(rev)
    if old_data:
      for metric in data:
        if metric not in old_data:
          old_data[metric] = {}
        old_data[metric].update(data[metric])
      return self.complete_update(objID, old_data)
    else:
      if "rev" not in data:
        data["rev"] = rev
      if src_rev is not None and "src-rev" not in data:
        data["src-rev"] = src_rev

      self.insert_data(data)

  def list_all_metrics(self):
    return list(self.client["CI"]["list_metrics"].find({}, {"_id": 0}))[0]["metrics"]

  def list_metrics(self):
    return set([_.split(".")[0] for _ in self.list_all_metrics()])

  def list_metrics_types(self, metric):
    tmetrics = [(_[0], _[1]) for _ in map(lambda x: x.split('.'), self.list_all_metrics())]
    metrics = {d[0]: [] for d in tmetrics}
    for k, v in tmetrics:
      metrics[k].append(v)
    return metrics[metric]

  def get_metric_values(self, metric_name):
    return self.table.find({}, {"rev": 1, "src-rev": {"$ifNull": ["$src-rev", None]}, "_id": 0, metric_name: 1}, sort = [("rev", pymongo.ASCENDING)])



class ReleasesClient(MyMongoClient):
  default_data = {"sync_mc": False, "sync_cdb": False, "processing_stats": False}
  def __init__(self, hostname, port, **kwargs):
    super().__init__(hostname, port, authSource="CI", **kwargs)
    self.index = "tag"

  def connect(self):
    super().connect()
    self.table = self.client["release_mgmt"]["na62fw"]

  def update_tag_data(self, tag, field, value):
    old_data, objID = self.prepare_update(tag)
    if old_data:
      old_data[field] = value
      return self.complete_update(objID, old_data)

  def insert_data(self, tag, data=None):
    if data is None:
      data = self.default_data
    data["tag"] = tag
    return super().insert_data(data)