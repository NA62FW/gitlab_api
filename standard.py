import os
import sys

def get_token(args, default_path=None):
  if args.token is None:
    token_path = default_path
    if token_path is None or not os.path.exists(token_path):
      # Unable to read gitlab
      print("No token found, unable to access gitlab")
      sys.exit(1)
    else:
      with open(token_path, "r") as fd:
        token = fd.read().strip()
  else:
    token = args.token

  return token