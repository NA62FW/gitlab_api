import requests
import inspect


class APIException(Exception):
  def __init__(self, result):
    self.result = result
    super().__init__()

  def __str__(self):
    return f"Unable to access {self.result.url}"


class Unauthorized(APIException):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def __str__(self):
    return super().__str__() + f": Permission denied [{self.result.status_code}]"


class NotFound(APIException):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def __str__(self):
    return super().__str__() + f": Not found [{self.result.status_code}]"

class Conflict(APIException):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def __str__(self):
    return super().__str__() + f": Conflict [{self.result.status_code}]"


class UnkownHTTPCode(APIException):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def __str__(self):
    return f"Unknown return code from {self.result.url} [{self.result.status_code}]"


def check_return(result, raw=False):
  if result.status_code == 200:
    return result if raw else result.json()
  elif result.status_code == 201:
    return result if raw else result.json()
  elif result.status_code == 204:
    return None
  elif result.status_code == 404:
    raise NotFound(result)
  elif result.status_code == 401:
    raise Unauthorized(result)
  elif result.status_code == 403:
    raise Unauthorized(result)
  elif result.status_code == 409:
    raise Conflict(result)
  else:
    raise UnkownHTTPCode(result)


def encoded_id(id):
  if not isinstance(id, int):
    return id.replace("/", "%2F")
  return id

class _GitlabBaseAPI(object):
  def __init__(self, instance, token, ssl=True, verbose=False):
    self.instance = instance
    self.token = token
    self.verbose = verbose

    self.header_base = {"PRIVATE-TOKEN": self.token}

    self.last_url = None
    self.last_data = None

  def _create_endpoints(self, classType):
    # List all variables git the class
    v = vars(classType)
    # Select only variables that are subclasses of _endpoint but not _endpoint itself
    endpoints = [(_, v[_]) for _ in v if inspect.isclass(v[_]) and _ != "_endpoint" and issubclass(v[_], _GitlabBaseAPI._endpoint)]
    # Create one instance of each in the class
    for ep in endpoints:
      setattr(self, ep[0][1:], ep[1](self))


  class _endpoint(object):
    def __init__(self, api, endpoint):
      self.api = api
      self.base_url = f"{api.base_url}/{endpoint}"

    def url(self, form={}, element=None):
      url_string = self.base_url
      if element:
        if isinstance(element, str) or isinstance(element, int):
          url_string = url_string + f"/{element}"
        elif isinstance(element, list):
          url_string = url_string + "/" + "/".join([str(_) for _ in element])
        else:
          raise TypeError("element must be a string, an int or a list")
      if len(form) > 0:
        formstring = "&".join([f"{_}={form[_]}" for _ in form])
        url_string = url_string + f"?{formstring}"
      return url_string

    def _get(self, url, raw=False):
      if self.api.verbose:
        print(f"Calling {url}")
      self.api.last_url = url
      result = requests.get(url, headers = self.api.header_base)
      return check_return(result, raw)

    def _post(self, url, data):
      if self.api.verbose:
        print(f"Calling {url} with data {data}")
      self.api.last_url = url
      self.api.last_data = data
      result = requests.post(url, json = data, headers = self.api.header_base)
      return check_return(result)

    def _delete(self, url):
      if self.api.verbose:
        print(f"Calling {url}")
      self.api.last_url = url
      result = requests.delete(url, headers = self.api.header_base)
      return check_return(result)


class GitlabInstanceAPI(_GitlabBaseAPI):
  def __init__(self, instance, token, ssl=True, verbose=False):
    super().__init__(instance, token, ssl=ssl, verbose=verbose)

    self._build_url(ssl)
    self._create_endpoints(GitlabInstanceAPI)

  def _build_url(self, ssl):
    protocol = "https" if ssl else "http"
    self.base_url = f"{protocol}://{self.instance}/api/v4/"

  class _project(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "projects")

    def get(self, id=None):
      if id:
        return self._get(self.url(element=encoded_id(id)))
      else:
        return self._get(self.url())

  class _group(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "groups")

    def get(self, id=None):
      if id:
        return self._get(self.url(element=encoded_id(id)))
      else:
        return self._get(self.url())


class GitlabAPI(_GitlabBaseAPI):
  def __init__(self, instance, token, project_id, ssl=True, verbose=False):
    super().__init__(instance, token, ssl=ssl, verbose=verbose)
    self.project_id = project_id

    self._build_url(ssl)
    self._create_endpoints(GitlabAPI)

  def _build_url(self, ssl):
    protocol = "https" if ssl else "http"
    encoded_project_id = self.project_id
    if not isinstance(self.project_id, int):
      encoded_project_id = self.project_id.replace("/", "%2F")
    self.base_url = f"{protocol}://{self.instance}/api/v4/projects/{encoded_project_id}"

  class _mr(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "merge_requests")

    def get(self, iid, raw=False):
      return self._get(self.url(form={"iids": iid}), raw)

    def notes(self, iid, note_iid=None):
      if note_iid:
        return self._get(self.url(element=[iid, "notes", note_iid]))
      else:
        return self._get(self.url(element=[iid, "notes"]))

    def create_note(self, iid, body):
      return self._post(self.url(element=[iid, "notes"]), data={"body": body})

  class _tag(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "repository/tags")

    def get(self, name=None, raw=False):
      if name:
        return self._get(self.url(element=name), raw)
      else:
        return self._get(self.url(), raw)

  class _milestones(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "milestones")

    def get(self, search=None, title=None, iid=None, state=None, raw=False):
      if search is None:
        search = {}
      if title and not "title" in search:
        search["title"] = title
      if state and not "state" in search:
        search["state"] = state
      if iid and len(search) > 0:
        raise ValueError("iid cannot be specified with other arguments")

      if iid:
        return self._get(self.url(element=iid), raw)
      else:
        return self._get(self.url(form=search), raw)

  class _pipelines(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "pipelines")

    def get(self, iid=None, search=None, raw=False):
      if iid and search:
        raise ValueError("iid or search must be specified, not both")

      if iid:
        return self._get(self.url(element=iid), raw)
      elif search:
        return self._get(self.url(form=search), raw)
      else:
        return self._get(self.url(), raw)

    def jobs(self, iid):
      return self._get(self.url(element=[iid, "jobs"]))

    def cancel(self, pipeline_id):
      return self._post(self.url(element=[pipeline_id, "cancel"]), {})

    def delete(self, pipeline_id):
      return self._delete(self.url(element=pipeline_id))

  class _jobs(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "jobs")

    def get(self, iid=None, raw=False):
      if iid:
        return self._get(self.url(element=iid), raw)
      else:
        return self._get(self.url(), raw)

    def artifacts(self, iid, path=None, raw=False):
      if path:
        return self._get(self.url(element=[iid, "artifacts", path]), raw)
      else:
        return self._get(self.url(element=[iid, "artifacts"]), raw)

  class _releases(_GitlabBaseAPI._endpoint):
    def __init__(self, api):
      super().__init__(api, "releases")

    def get(self, name=None, raw=False):
      if name:
        return self._get(self.url(element=name), raw)
      else:
        return self._get(self.url(), raw)

    def create(self, tag_name, data=None, **kwargs):
      if data is None:
        data = {}
      data.update(kwargs)
      data["tag_name"] = tag_name
      return self._post(self.url(), data=data)
